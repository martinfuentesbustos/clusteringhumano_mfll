package ar.edu.ungs.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GrafoPersonasTest.class, PersonaTest.class, SimilaridadTest.class, AristaTest.class, VerticeTest.class, PromediosTest.class })
public class AllTests {

}
