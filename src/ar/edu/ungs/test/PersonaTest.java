package ar.edu.ungs.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.ungs.clusteringHumano.Persona;

public class PersonaTest {

	Persona persona1;
	Persona persona2;
	Persona persona3;

	@Before
	public void setUp() {
		persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		persona2 = new Persona(2, "Ada Lovelace", 3, 4, 5, 1);
		persona3 = new Persona(2, "Ada Lovelace", 3, 4, 5, 1);
	}

	@Test
	public void personasIgualesTest() {
		assertTrue(persona3.equals(persona3));
	}
	
	@Test
	public void personasDistintasTest() {
		assertFalse(persona1.equals(persona2));
	}
	
	@Test
	public void personasDistintasNullTest() {
		assertFalse(persona1.equals(null));
	}
	
	@After
	public void tearDown() {
		persona1 = null;
		persona2 = null;
		persona3 = null;
	}

}
