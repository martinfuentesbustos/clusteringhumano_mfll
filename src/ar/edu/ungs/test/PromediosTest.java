package ar.edu.ungs.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ar.edu.ungs.clusteringHumano.Algoritmos;
import ar.edu.ungs.clusteringHumano.Persona;

public class PromediosTest {

	@Test
	public void cienciaTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Set <Persona> grupo = new HashSet<Persona>();
		grupo.add(nombre);
		grupo.add(nombre2);
		
		assertEquals(2, Algoritmos.promedioCiencia(grupo),0.1);
	}
	
	@Test
	public void deportesTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Set <Persona> grupo = new HashSet<Persona>();
		grupo.add(nombre);
		grupo.add(nombre2);
		
		
		assertEquals(0.5, Algoritmos.promedioDeportes(grupo),0.1);
	}
	
	@Test
	public void musicaTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Set <Persona> grupo = new HashSet<Persona>();
		grupo.add(nombre);
		grupo.add(nombre2);
		
		
		assertEquals(3.5, Algoritmos.promedioMusica(grupo),0.1);
	}

	
	@Test
	public void espectaculoTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Set <Persona> grupo = new HashSet<Persona>();
		grupo.add(nombre);
		grupo.add(nombre2);
		
		
		assertEquals(2.5, Algoritmos.promedioEspectaculo(grupo),0.1);
	}

	

}
