package ar.edu.ungs.test;

import static org.junit.Assert.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;

import org.junit.Test;

import ar.edu.ungs.clusteringHumano.gui.componentes.Arista;

public class AristaTest {

	@Test
	public void puntoMedioTest() {
		Point pInicial= new Point(35, 40);
		Point pFinal= new Point(100, 120);
		Arista arista = new Arista(3, pInicial, pFinal, Color.BLUE, new BasicStroke());
		
		Point pMedio = arista.obtenerPuntoMedio(pInicial, pFinal);
		Point pEsperado = new Point(67, 80);
		assertEquals(pEsperado, pMedio);
	}
	
	@Test
	public void conectarPuntosTest() {
		Point pInicial= new Point(35, 40);
		Point pFinal= new Point(100, 120);
		
		Arista arista = new Arista(3, Color.BLUE, new BasicStroke());
		arista.conectarPuntos(pInicial, pFinal);

		Arista aristaEsperada = new Arista(3, pInicial, pFinal, Color.BLUE, new BasicStroke());
		
		assertEquals(aristaEsperada, arista);
	}

}
