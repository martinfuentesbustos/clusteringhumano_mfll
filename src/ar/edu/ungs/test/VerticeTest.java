package ar.edu.ungs.test;

import static org.junit.Assert.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;

import org.junit.Test;

import ar.edu.ungs.clusteringHumano.Persona;
import ar.edu.ungs.clusteringHumano.gui.componentes.Vertice;

public class VerticeTest {

	
	@Test
	public void otenerPosicionCentroTest() {
		Persona persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		Integer posicion = Vertice.POS_CENTRO;
		Vertice v1 = new Vertice(50, 40, 200, 340, Color.BLACK, new BasicStroke());
		v1.setPersona(persona1);
		Point puntoFinal = v1.obtenerPosicionPunto(posicion);
		Point puntoEsperado = new Point(220, 140);
		
		assertEquals(puntoEsperado, puntoFinal);
	}
	
	@Test
	public void otenerPosicionArribaTest() {
		Persona persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		Integer posicion = Vertice.POS_ARRIBA;
		Vertice v1 = new Vertice(50, 40, 200, 340, Color.BLACK, new BasicStroke());
		v1.setPersona(persona1);
		Point puntoFinal = v1.obtenerPosicionPunto(posicion);
		Point puntoEsperado = new Point(50, 30);
		
		assertEquals(puntoEsperado, puntoFinal);
	}
	
	@Test
	public void obtenerPosicionAbajoTest() {
		Persona persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		Integer posicion = Vertice.POS_ABAJO;
		Vertice v1 = new Vertice(50, 40, 200, 340, Color.BLACK, new BasicStroke());
		v1.setPersona(persona1);
		Point puntoFinal = v1.obtenerPosicionPunto(posicion);
		Point puntoEsperado = new Point(50, 256);
		
		assertEquals(puntoEsperado, puntoFinal);
	}
	
	@Test
	public void obtenerPosicionDerechaTest() {
		Persona persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		Integer posicion = Vertice.POS_DERECHA;
		Vertice v1 = new Vertice(50, 40, 200, 340, Color.BLACK, new BasicStroke());
		v1.setPersona(persona1);
		Point puntoFinal = v1.obtenerPosicionPunto(posicion);
		Point puntoEsperado = new Point(400, 140);
		
		assertEquals(puntoEsperado, puntoFinal);
	}
	
	@Test
	public void obtenerPosicionInquierdaTest() {
		Persona persona1 = new Persona(1, "Alan Turing", 1, 1, 1, 1);
		Integer posicion = Vertice.POS_IZQUIERDA;
		Vertice v1 = new Vertice(50, 40, 200, 340, Color.BLACK, new BasicStroke());
		v1.setPersona(persona1);
		Point puntoFinal = v1.obtenerPosicionPunto(posicion);
		Point puntoEsperado = new Point(40, 140);
		
		assertEquals(puntoEsperado, puntoFinal);
	}

}
