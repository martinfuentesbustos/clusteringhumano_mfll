package ar.edu.ungs.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ar.edu.ungs.clusteringHumano.Algoritmos;
import ar.edu.ungs.clusteringHumano.Persona;

public class SimilaridadTest {

	@Test
	public void similaridadTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		
		assertEquals(7, Algoritmos.calcularSimilaridad(nombre, nombre2));
	}
	
	@Test(expected = NullPointerException.class)
	public void similaridadNullTest() {
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		
		Algoritmos.calcularSimilaridad(nombre, null);
	}

}
