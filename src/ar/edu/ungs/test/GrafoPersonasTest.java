package ar.edu.ungs.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import ar.edu.ungs.clusteringHumano.GrafoPersonas;
import ar.edu.ungs.clusteringHumano.Persona;

public class GrafoPersonasTest {

	@Test
	public void existePersonaTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		grafo.agregarPersona(nombre);
		
		assertTrue(grafo.existePersona(nombre));
	}
	
	@Test
	public void noExistePersonaTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Natalia",1,1,1,1);
		grafo.agregarPersona(nombre);
		
		assertFalse(grafo.existePersona(nombre2));
	}
	
	
	@Test
	public void agregarPersonaTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		grafo.agregarPersona(nombre);

		assertEquals(1, grafo.tamanio());;
	}
	
	@Test
	public void eliminarPersonaTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		grafo.agregarPersona(nombre);
		grafo.eliminarPersona(nombre);
		
		assertEquals(0, grafo.tamanio());;
	}
	
	
	
	@Test
	public void relacionExistenTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",2,2,2,2);
		
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		
		grafo.agregarRelacion(nombre, nombre2);

		assertTrue(grafo.existeRelacion(nombre, nombre2));
	}
	
	@Test
	public void relacionNoExistenTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",2,2,2,2);
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);

		assertFalse(grafo.existeRelacion(nombre, nombre2));
	}
	
	
	@Test
	public void relacionInversaTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",2,2,2,2);
		
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		
		grafo.agregarRelacion(nombre, nombre2);

		assertTrue(grafo.existeRelacion(nombre2, nombre));
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void aristaBucle() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		grafo.agregarPersona(nombre);

		grafo.agregarRelacion(nombre, nombre);
	}
	
	@Test
	public void completarGrafoTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",2,2,2,2);
		Persona nombre3 = new Persona(3,"Luz",3,3,3,3);
		Persona nombre4 = new Persona(4,"Maria",4,4,4,4);
		Persona nombre5 = new Persona(5,"Soledad",5,5,5,5);
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		grafo.agregarPersona(nombre3);
		grafo.agregarPersona(nombre4);
		grafo.agregarPersona(nombre5);
		
		grafo.completarGrafo();
		
		assertTrue(grafo.existeRelacion(nombre, nombre2));
		assertTrue(grafo.existeRelacion(nombre, nombre3));
		assertTrue(grafo.existeRelacion(nombre, nombre4));
		assertTrue(grafo.existeRelacion(nombre, nombre5));
		assertTrue(grafo.existeRelacion(nombre2, nombre3));
		assertTrue(grafo.existeRelacion(nombre2, nombre4));
		assertTrue(grafo.existeRelacion(nombre2, nombre5));
		assertTrue(grafo.existeRelacion(nombre3, nombre4));
		assertTrue(grafo.existeRelacion(nombre3, nombre5));
		assertTrue(grafo.existeRelacion(nombre4, nombre5));
		
	}
	
	@Test
	public void vecinoAfinTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",5,5,5,5);
		Persona nombre3 = new Persona(3,"Luz",1,1,1,1);
		Persona nombre4 = new Persona(4,"Maria",4,3,2,3);
		Persona nombre5 = new Persona(5,"Soledad",4,4,4,4);
		
		ArrayList<Persona> marcados = new ArrayList<Persona>();
		marcados.add(nombre2);
		marcados.add(nombre5);
		
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		grafo.agregarPersona(nombre3);
		grafo.agregarPersona(nombre4);
		grafo.agregarPersona(nombre5);
		grafo.completarGrafo();
		
		assertEquals(nombre3, grafo.vecinoAfin(nombre,marcados));
		
	}
	
	
	@Test
	public void grafoConexoTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		
		Persona nombre = new Persona(1,"Natalia",0,4,2,4);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Persona nombre3 = new Persona(3,"Luz",2,2,4,3);
		Persona nombre4 = new Persona(4,"Maria",3,1,1,2);
		Persona nombre5 = new Persona(5,"Soledad",4,0,0,1);
		
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		grafo.agregarPersona(nombre3);
		grafo.agregarPersona(nombre4);
		grafo.agregarPersona(nombre5);

		grafo.completarGrafo();
		assertTrue(grafo.esConexo());
		
	}
	
	@Test
	public void grafoNoConexoTest() {
		GrafoPersonas grafo = new GrafoPersonas();
		
		Persona nombre = new Persona(1,"Natalia",0,4,2,0);
		Persona nombre2 = new Persona(2,"Paula",1,3,3,4);
		Persona nombre3 = new Persona(3,"Luz",2,2,4,3);
		Persona nombre4 = new Persona(4,"Maria",3,1,1,2);
		Persona nombre5 = new Persona(5,"Soledad",4,0,0,1);
		
		grafo.agregarPersona(nombre);
		grafo.agregarPersona(nombre2);
		grafo.agregarPersona(nombre3);
		grafo.agregarPersona(nombre4);
		grafo.agregarPersona(nombre5);

		grafo.agregarRelacion(nombre, nombre2);
		grafo.agregarRelacion(nombre, nombre3);
		grafo.agregarRelacion(nombre4, nombre5);
		
		assertFalse(grafo.esConexo());
	}

/*
	@Test
	public void dameUnVerticeTest() {
		GrafoDePersonas grafo = new GrafoDePersonas();
		Persona nombre = new Persona(1,"Natalia",1,1,1,1);
		Persona nombre2 = new Persona(2,"Paula",2,2,2,2);
		grafo.agregarVertice(nombre);
		grafo.agregarVertice(nombre2);
		
		assertTrue(grafo.dameUnVertice().equals(nombre2) || grafo.dameUnVertice().equals(nombre2));
		
	} */
	
}
