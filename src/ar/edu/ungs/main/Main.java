package ar.edu.ungs.main;

import java.awt.EventQueue;

import ar.edu.ungs.clusteringHumano.gui.MenuPrincipalGUI;

public class Main {

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipalGUI window = new MenuPrincipalGUI();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
