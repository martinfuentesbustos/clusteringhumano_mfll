package ar.edu.ungs.clusteringHumano;

public class Persona {

	private int id;
	private String nombre;
	private int indiceDeportes;
	private int indiceMusica;
	private int indiceEspectaculos;
	private int indiceCiencia;

	public Persona(int id, String nombre, int deportes, int musica, int espectaculos, int ciencia) {
		this.id = id;
		this.nombre = nombre;
		this.indiceDeportes = deportes;
		this.indiceMusica = musica;
		this.indiceEspectaculos = espectaculos;
		this.indiceCiencia = ciencia;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}

	public int getIndiceDeportes() {
		return indiceDeportes;
	}

	public int getIndiceMusica() {
		return indiceMusica;
	}

	public int getIndiceEspectaculos() {
		return indiceEspectaculos;
	}

	public int getIndiceCiencia() {
		return indiceCiencia;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return nombre;
	}

}
