package ar.edu.ungs.clusteringHumano.gui.componentes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JPanel;

import ar.edu.ungs.clusteringHumano.Algoritmos;
import ar.edu.ungs.clusteringHumano.GrafoPersonas;
import ar.edu.ungs.clusteringHumano.Persona;

public class DibujoArbol {

	private GrafoPersonas arbol;
	private static int indice;
	private static int cantidadVertices;
	private static final int DESPLAZAMIENTO_GRAFICO_HORIZONTAL = 190;
	private static final int DESPLAZAMIENTO_GRAFICO_VERTICAL = 170;

	public DibujoArbol(GrafoPersonas arbol) {
		this.arbol = arbol;
		indice = 1;
		cantidadVertices = arbol.tamanio();
	}

	private Vertice obtenerSiguenteVertice() {
		Vertice vertice = null;
		double angulo = (2 * Math.PI) / cantidadVertices;
		int deltaX = 0;
		int deltaY = 0;

		int posicionTexto = Vertice.POS_ARRIBA;

		if (indice <= cantidadVertices) {

			if (Math.cos(angulo * indice) <= 0 && Math.sin(angulo * indice) <= 0) {
				deltaX = 0 + DESPLAZAMIENTO_GRAFICO_HORIZONTAL;
				deltaY = 0 + DESPLAZAMIENTO_GRAFICO_VERTICAL;
			}

			if (Math.cos(angulo * indice) <= 0 && Math.sin(angulo * indice) > 0) {
				deltaX = 0 + DESPLAZAMIENTO_GRAFICO_HORIZONTAL;
				deltaY = -20 + DESPLAZAMIENTO_GRAFICO_VERTICAL;
				posicionTexto = Vertice.POS_ABAJO;
			}

			if (Math.cos(angulo * indice) > 0 && Math.sin(angulo * indice) <= 0) {
				deltaX = -20 + DESPLAZAMIENTO_GRAFICO_HORIZONTAL;
				deltaY = 0 + DESPLAZAMIENTO_GRAFICO_VERTICAL;
			}

			if (Math.cos(angulo * indice) > 0 && Math.sin(angulo * indice) > 0) {
				deltaX = -20 + DESPLAZAMIENTO_GRAFICO_HORIZONTAL;
				deltaY = -20 + DESPLAZAMIENTO_GRAFICO_VERTICAL;
			}

			Point p = new Point((int) (deltaX + Math.cos(angulo * indice) * 150),
					(int) (deltaY + Math.sin(angulo * indice) * 150));

			vertice = new Vertice(p.x, p.y, 20, 20, Color.RED, new BasicStroke(2.0f));
			vertice.setPosicionTexto(posicionTexto);

			indice++;
		}

		return vertice;
	}


	public void dibujarse(JPanel lienzo) {
		Vertice origen = null;
		Vertice destino = null;

		ArrayList<Persona> personasDibujadas = new ArrayList<Persona>();
		ArrayList<Vertice> verticesDibujados = new ArrayList<Vertice>();

		for (Persona persona : arbol.obtenerTodos()) {

			if (!personasDibujadas.contains(persona)) {
				origen = obtenerSiguenteVertice();
				origen.setPersona(persona);
				origen.dibujarse(lienzo);
				personasDibujadas.add(persona);
				verticesDibujados.add(origen);
			} else {
				origen = obtenerVerticePersona(persona, verticesDibujados);
			}

			HashSet<Persona> vecinosPersona = arbol.devolverVecinos(persona);

			for (Persona vecino : vecinosPersona) {

				if (!personasDibujadas.contains(vecino)) {
					destino = obtenerSiguenteVertice();
					destino.setPersona(vecino);
					destino.dibujarse(lienzo);
					personasDibujadas.add(vecino);
					verticesDibujados.add(destino);
				} else {
					destino = obtenerVerticePersona(vecino, verticesDibujados);
				}

				int peso = Algoritmos.calcularSimilaridad(persona, vecino);

				Arista arista = new Arista(peso, Color.BLACK, new BasicStroke(1.5f));
				arista.conectarPuntos(origen.obtenerPosicionPunto(Vertice.POS_CENTRO),
						destino.obtenerPosicionPunto(Vertice.POS_CENTRO));

				arista.dibujarse(lienzo);
			}

		}
	}

	private Vertice obtenerVerticePersona(Persona persona, ArrayList<Vertice> vertices) {

		for (Vertice vertice : vertices) {
			if (vertice.getPersona().equals(persona)) {
				return vertice;
			}
		}
		return null;
	}
}
