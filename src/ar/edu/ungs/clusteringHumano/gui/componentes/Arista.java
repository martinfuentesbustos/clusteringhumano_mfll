package ar.edu.ungs.clusteringHumano.gui.componentes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Stroke;

import javax.swing.JPanel;

import ar.edu.ungs.clusteringHumano.utils.MotorGrafico;

public class Arista {

	private int peso;
	
	private Point puntoInicial;
	
	private Point puntoFinal;
	
	private Color color;
	
	private Stroke trazo;

	public Arista() {}

	public Arista(int peso, Color color, Stroke trazo) {
		super();
		this.peso = peso;
		this.color = color;
		this.trazo = trazo;
	}

	
	public Arista(int peso, Point puntoInicial, Point puntoFinal, Color color, Stroke trazo) {
		super();
		this.peso = peso;
		this.puntoInicial = puntoInicial;
		this.puntoFinal = puntoFinal;
		this.color = color;
		this.trazo = trazo;
	}

	
	public void dibujarse(JPanel padre) {
		Point puntoMedio = obtenerPuntoMedio(puntoInicial, puntoFinal);
		Graphics g = padre.getGraphics();
		MotorGrafico.ponerColorTrazo(g, color);
		MotorGrafico.ponerTrazo(g, trazo);
		MotorGrafico.dibujarLinea(g, puntoInicial.x, puntoFinal.x, puntoInicial.y, puntoFinal.y);
		MotorGrafico.escribirTexto(g, this.peso+"", puntoMedio.x, puntoMedio.y);
		
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public Point getPuntoInicial() {
		return puntoInicial;
	}

	public void setPuntoInicial(Point puntoInicial) {
		this.puntoInicial = puntoInicial;
	}

	public Point getPuntoFinal() {
		return puntoFinal;
	}

	public void setPuntoFinal(Point puntoFinal) {
		this.puntoFinal = puntoFinal;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Stroke getTrazo() {
		return trazo;
	}

	public void setTrazo(Stroke trazo) {
		this.trazo = trazo;
	}
	
	public Point obtenerPuntoMedio(Point pInicial, Point pFinal) {
		int xMedia = (pInicial.x + pFinal.x) / 2;
		int yMedia = (pInicial.y + pFinal.y) / 2;
		return new Point(xMedia, yMedia);
	}
	
	public Arista conectarPuntos(Point pInicial, Point pFinal) {
		
		this.setPuntoInicial(pInicial);
		this.setPuntoFinal(pFinal);
		
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (puntoFinal == null) {
			if (other.puntoFinal != null)
				return false;
		} else if (!puntoFinal.equals(other.puntoFinal))
			return false;
		if (puntoInicial == null) {
			if (other.puntoInicial != null)
				return false;
		} else if (!puntoInicial.equals(other.puntoInicial))
			return false;
		return true;
	}
	
}
