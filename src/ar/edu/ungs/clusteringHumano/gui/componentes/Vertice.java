package ar.edu.ungs.clusteringHumano.gui.componentes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Stroke;

import javax.swing.JPanel;

import ar.edu.ungs.clusteringHumano.Persona;
import ar.edu.ungs.clusteringHumano.utils.Constantes;
import ar.edu.ungs.clusteringHumano.utils.MotorGrafico;

public class Vertice {

	public static final Integer POS_CENTRO = 0;
	public static final Integer POS_ARRIBA = 1;
	public static final Integer POS_ABAJO = -1;
	public static final Integer POS_DERECHA = 2;
	public static final Integer POS_IZQUIERDA = -2;
	
	private Persona persona;
	
	private int x;
	
	private int y;
	
	private int alto;
	
	private int ancho;
	
	private Color color;
	
	private Stroke trazo;
	
	private Integer posicionTexto;
	
	public Vertice(Persona persona) {
		this.persona = persona;
	}

	public Vertice(int x, int y, int alto, int ancho, Color color, Stroke trazo) {
		super();
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.color = color;
		this.trazo = trazo;
		this.posicionTexto = POS_ARRIBA;
	}

	
	public void dibujarse(JPanel padre) {
		Point puntoTexto = obtenerPosicionPunto(this.posicionTexto);
		Graphics g = padre.getGraphics();
		MotorGrafico.ponerColorTrazo(g, color);
		MotorGrafico.ponerTrazo(g, trazo);
		MotorGrafico.dibujarCirculo(g,this.x, this.y, this.alto, this.ancho);
		MotorGrafico.escribirTexto(g, this.persona.getNombre(), puntoTexto.x, puntoTexto.y);
		
	}

	public void dibujarseCustom(JPanel padre, int x, int y, int alto, int ancho, Color color, Stroke trazo) {
		Graphics g = padre.getGraphics();
		MotorGrafico.ponerColorTrazo(g, color);
		MotorGrafico.ponerTrazo(g, trazo);
		MotorGrafico.dibujarCirculo(g,10, 30, 120, 120);
	}


	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Stroke getTrazo() {
		return trazo;
	}

	public void setTrazo(Stroke trazo) {
		this.trazo = trazo;
	}

	public Integer getPosicionTexto() {
		return posicionTexto;
	}

	public void setPosicionTexto(Integer posicionTexto) {
		this.posicionTexto = posicionTexto;
	}
	
	public Point obtenerPosicionPunto(Integer posicion) {
		Point coordenada = null;
		switch (posicion) {
		case 0:
			coordenada = new Point(x + ancho/2, y + alto/2);
			break;
		case 1:
			coordenada = new Point(x, y - Constantes.ESPACIO_DEFAULT);
			break;
		case -1:
			coordenada = new Point(x, y + alto + Constantes.ESPACIO_DEFAULT + Constantes.ALTO_CARACTER);
			break;
		case 2:
			coordenada = new Point(x + ancho + Constantes.ESPACIO_DEFAULT, y+alto/2);
			break;
		case -2:
			coordenada = new Point(x - Constantes.ESPACIO_DEFAULT, y + alto/2);
			break;			
		default:
			break;
		}
		return coordenada;
	}
	
}
