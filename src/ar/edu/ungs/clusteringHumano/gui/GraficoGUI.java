package ar.edu.ungs.clusteringHumano.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.ungs.clusteringHumano.GrafoPersonas;
import ar.edu.ungs.clusteringHumano.gui.componentes.DibujoArbol;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GraficoGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelGrafico;

	/**
	 * Create the frame.
	 */
	public GraficoGUI(GrafoPersonas arbol) {

		setTitle("Gr�fico final");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 441, 465);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelGrafico = new JPanel();
		panelGrafico.setBounds(10, 11, 405, 373);

		contentPane.add(panelGrafico);

		setContentPane(contentPane);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				dibujarGrafico(arbol);
			}
		});
	}

	private void dibujarGrafico(GrafoPersonas arbol) {
		DibujoArbol dibujoArbol = new DibujoArbol(arbol);
		dibujoArbol.dibujarse(panelGrafico);
	}
}
