package ar.edu.ungs.clusteringHumano.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import ar.edu.ungs.clusteringHumano.Algoritmos;
import ar.edu.ungs.clusteringHumano.GrafoPersonas;
import ar.edu.ungs.clusteringHumano.Persona;
import ar.edu.ungs.clusteringHumano.utils.Constantes;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;

public class MenuPrincipalGUI extends JFrame {
	private static final long serialVersionUID = 1L;

	private GrafoPersonas grafoPersonas;
	private GrafoPersonas arbolMinimo;
	private JTextField textFieldNombre;
	private JList<Persona> listPersonas;
	private ButtonGroup buttonGroupDeportes;
	private ButtonGroup buttonGroupMusica;
	private ButtonGroup buttonGroupEspectaculos;
	private ButtonGroup buttonGroupCiencia;
	private JPanel panelMenu;
	private JPanel panelEstado;
	private JPanel panelDatos;
	private JPanel panelResultado;
	private JPanel panelResultadoCompleto;
	private JLabel lblEstado;
	private JLabel lblInfoPersona;
	private JLabel lblResultado;
	private JLabel lblResultadoInfo;
	private JLabel lblCargarPersonas;
	private JLabel lblEjecutarProceso;
	private JPanel panelFormulario;
	private JPanel panelIntereses;
	private JLabel lblIntersEnDeportes;
	private JLabel lblMsica;
	private JLabel lblEspectculos;
	private JLabel lblCiencia;
	private JLabel lblNombre;
	private JLabel lblResultadoTitulo;
	private JLabel lblVerGrfico;
	private JButton btnAgregar;
	private JButton btnCalcularGrupos;
	private JButton btnCargarPersona;
	private JButton btnMostrarGraficos;
	private JRadioButton d1;
	private JRadioButton d2;
	private JRadioButton d3;
	private JRadioButton d4;
	private JRadioButton d5;
	private JRadioButton m1;
	private JRadioButton m2;
	private JRadioButton m3;
	private JRadioButton m4;
	private JRadioButton m5;
	private JRadioButton e1;
	private JRadioButton e2;
	private JRadioButton e3;
	private JRadioButton e4;
	private JRadioButton e5;
	private JRadioButton c1;
	private JRadioButton c2;
	private JRadioButton c3;
	private JRadioButton c4;
	private JRadioButton c5;
	private JTextPane txtPaneInfoLista;
	
	/**
	 * Create the application.
	 */
	public MenuPrincipalGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MenuPrincipalGUI.class.getResource("/images/cluster_icon.png")));
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setTitle(Constantes.NOMBRE_APLICACION);
		this.setBounds(100, 100, 797, 472);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		this.setLocationRelativeTo(null);
		
		
		grafoPersonas = new GrafoPersonas();
		listPersonas = new JList<Persona>(new DefaultListModel<Persona>());
		
		panelMenu = new JPanel();
		panelMenu.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Men\u00FA",
				TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		panelMenu.setBounds(4, 0, 133, 402);
		panelMenu.setLayout(null);
		
		
		buttonGroupDeportes = new ButtonGroup();
		buttonGroupMusica = new ButtonGroup();
		buttonGroupEspectaculos = new ButtonGroup();
		buttonGroupCiencia = new ButtonGroup();

		btnCalcularGrupos = new JButton("");
		btnCalcularGrupos.setIcon(new ImageIcon(MenuPrincipalGUI.class.getResource("/images/icon_run.png")));
		btnCalcularGrupos.setEnabled(false);
		btnCalcularGrupos.setBounds(16, 251, 100, 100);
		panelMenu.add(btnCalcularGrupos);

		btnCargarPersona = new JButton("");
		btnCargarPersona.setIcon(new ImageIcon(MenuPrincipalGUI.class.getResource("/images/icon_personas.png")));
		btnCargarPersona.setBounds(15, 54, 100, 100);
		panelMenu.add(btnCargarPersona);
		
		lblCargarPersonas = new JLabel("Cargar personas");
		lblCargarPersonas.setHorizontalAlignment(SwingConstants.CENTER);
		lblCargarPersonas.setBounds(13, 155, 100, 14);
		panelMenu.add(lblCargarPersonas);
		
		lblEjecutarProceso = new JLabel("Ejecutar proceso");
		lblEjecutarProceso.setHorizontalAlignment(SwingConstants.CENTER);
		lblEjecutarProceso.setBounds(16, 353, 100, 14);
		panelMenu.add(lblEjecutarProceso);
		
		panelFormulario = new JPanel();
		panelFormulario.setVisible(false);
		panelFormulario.setBounds(147, 0, 624, 405);
		panelFormulario.setLayout(null);
			
		listPersonas.setToolTipText("");
		listPersonas.setBounds(437, 55, 182, 200);
		panelFormulario.add(listPersonas);
		listPersonas.setBackground(SystemColor.menu);
		listPersonas.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)),
					"Personas agregadas", TitledBorder.CENTER, TitledBorder.BOTTOM, null, new Color(128, 128, 128)));
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem mntmBorrar = new JMenuItem("Borrar");
		popupMenu.add(mntmBorrar);
		
		listPersonas.setComponentPopupMenu(popupMenu);
		
						
		panelIntereses = new JPanel();
		panelIntereses.setLayout(null);
		panelIntereses.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)),
							"Intereses", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		panelIntereses.setBounds(0, 102, 427, 225);
		panelFormulario.add(panelIntereses);
		lblIntersEnDeportes = new JLabel("Deportes");
		lblIntersEnDeportes.setBounds(24, 38, 57, 14);
		panelIntereses.add(lblIntersEnDeportes);
										
		lblMsica = new JLabel("M�sica");
		lblMsica.setBounds(24, 85, 57, 14);
		panelIntereses.add(lblMsica);
												
		lblEspectculos = new JLabel("Espect�culos");
		lblEspectculos.setBounds(24, 139, 78, 14);
		panelIntereses.add(lblEspectculos);
														
		lblCiencia = new JLabel("Ciencia");
		lblCiencia.setBounds(24, 188, 78, 14);
		panelIntereses.add(lblCiencia);
																
		d1 = new JRadioButton("1");
		d1.setActionCommand(d1.getText());
		buttonGroupDeportes.add(d1);
		d1.setBounds(135, 34, 37, 23);
		panelIntereses.add(d1);
																		
		d2 = new JRadioButton("2");
		d2.setActionCommand(d2.getText());
		buttonGroupDeportes.add(d2);
		d2.setBounds(174, 34, 37, 23);
		panelIntereses.add(d2);
																				
		d3 = new JRadioButton("3");
		d3.setActionCommand(d3.getText());
		buttonGroupDeportes.add(d3);
		d3.setBounds(213, 34, 37, 23);
		panelIntereses.add(d3);
																						
		d4 = new JRadioButton("4");
		d4.setActionCommand(d4.getText());
		buttonGroupDeportes.add(d4);
		d4.setBounds(252, 34, 37, 23);
		panelIntereses.add(d4);
																								
		d5 = new JRadioButton("5");
		d5.setActionCommand(d5.getText());
		buttonGroupDeportes.add(d5);
		d5.setBounds(291, 34, 37, 23);
		panelIntereses.add(d5);
																										
		m1 = new JRadioButton("1");
		m1.setActionCommand(m1.getText());
		buttonGroupMusica.add(m1);
		m1.setBounds(135, 81, 37, 23);
		panelIntereses.add(m1);
																												
		m2 = new JRadioButton("2");
		m2.setActionCommand(m2.getText());
		buttonGroupMusica.add(m2);
		m2.setBounds(174, 81, 37, 23);
		panelIntereses.add(m2);
																														
		m3 = new JRadioButton("3");
		m3.setActionCommand(m3.getText());
		buttonGroupMusica.add(m3);
		m3.setBounds(213, 81, 37, 23);
		panelIntereses.add(m3);
																																
		m4 = new JRadioButton("4");
		m4.setActionCommand(m4.getText());
		buttonGroupMusica.add(m4);
		m4.setBounds(252, 81, 37, 23);
		panelIntereses.add(m4);
																																		
		m5 = new JRadioButton("5");
		m5.setActionCommand(m5.getText());
		buttonGroupMusica.add(m5);
		m5.setBounds(291, 81, 37, 23);
		panelIntereses.add(m5);
																																				
		e1 = new JRadioButton("1");
		e1.setActionCommand(e1.getText());
		buttonGroupEspectaculos.add(e1);
		e1.setBounds(135, 135, 37, 23);
		panelIntereses.add(e1);
																																						
		e2 = new JRadioButton("2");
		e2.setActionCommand(e2.getText());
		buttonGroupEspectaculos.add(e2);
		e2.setBounds(174, 135, 37, 23);
		panelIntereses.add(e2);
																																								
		e3 = new JRadioButton("3");
		e3.setActionCommand(e3.getText());
		buttonGroupEspectaculos.add(e3);
		e3.setBounds(213, 135, 37, 23);
		panelIntereses.add(e3);
																																										
		e4 = new JRadioButton("4");
		e4.setActionCommand(e4.getText());
		buttonGroupEspectaculos.add(e4);
		e4.setBounds(252, 135, 37, 23);
		panelIntereses.add(e4);
																																												
		e5 = new JRadioButton("5");
		e5.setActionCommand(e5.getText());
		buttonGroupEspectaculos.add(e5);
		e5.setBounds(291, 135, 37, 23);
		panelIntereses.add(e5);
																																														
		c1 = new JRadioButton("1");
		c1.setActionCommand(c1.getText());
		buttonGroupCiencia.add(c1);
		c1.setBounds(135, 184, 37, 23);
		panelIntereses.add(c1);
																																																
		c2 = new JRadioButton("2");
		c2.setActionCommand(c2.getText());
		buttonGroupCiencia.add(c2);
		c2.setBounds(174, 184, 37, 23);
		panelIntereses.add(c2);
																																																		
		c3 = new JRadioButton("3");
		c3.setActionCommand(c3.getText());
		buttonGroupCiencia.add(c3);
		c3.setBounds(213, 184, 37, 23);
		panelIntereses.add(c3);
																																																				
		c4 = new JRadioButton("4");
		c4.setActionCommand(c4.getText());
		buttonGroupCiencia.add(c4);
		c4.setBounds(252, 184, 37, 23);
		panelIntereses.add(c4);
																																																						
		c5 = new JRadioButton("5");
		c5.setActionCommand(c5.getText());
		buttonGroupCiencia.add(c5);
		c5.setBounds(291, 184, 37, 23);
		panelIntereses.add(c5);
																																																								
		panelDatos = new JPanel();
		panelDatos.setBorder(new TitledBorder(
		new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)),
			"Datos Personales", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
		panelDatos.setBounds(0, 0, 427, 96);
		panelFormulario.add(panelDatos);
		panelDatos.setLayout(null);
																																																										
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(20, 44, 83, 14);
		panelDatos.add(lblNombre);
																																																												
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(123, 41, 212, 20);
		panelDatos.add(textFieldNombre);
		textFieldNombre.setColumns(10);
																																																														
		btnAgregar = new JButton("");
		btnAgregar.setIcon(new ImageIcon(MenuPrincipalGUI.class.getResource("/images/add.png")));
		btnAgregar.setBounds(184, 330, 70, 70);
		panelFormulario.add(btnAgregar);
		
		lblInfoPersona = new JLabel("");
		lblInfoPersona.setBounds(437, 259, 180, 124);
		panelFormulario.add(lblInfoPersona);
		lblInfoPersona.setVisible(false);
		lblInfoPersona.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Info ",
		TitledBorder.CENTER, TitledBorder.BOTTOM, null, new Color(128, 128, 128)));
		
		txtPaneInfoLista = new JTextPane();
		StyledDocument doc = txtPaneInfoLista.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		txtPaneInfoLista.setOpaque(false);
		txtPaneInfoLista.setText("Haz clic en la persona para ver su informac\u00F3n detallada");
		txtPaneInfoLista.setBounds(440, 6, 174, 50);
		panelFormulario.add(txtPaneInfoLista);
		
		panelResultado = new JPanel();
		panelResultado.setVisible(false);
		panelResultado.setBounds(143, 4, 628, 398);
		panelResultado.setLayout(null);
		
		panelResultadoCompleto = new JPanel();
		panelResultadoCompleto.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelResultadoCompleto.setBounds(10, 3, 428, 393);
		panelResultado.add(panelResultadoCompleto);
		panelResultadoCompleto.setLayout(null);
		
		lblResultadoTitulo = new JLabel("Resultado");
		lblResultadoTitulo.setBounds(0, 0, 415, 28);
		panelResultadoCompleto.add(lblResultadoTitulo);
		lblResultadoTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultadoTitulo.setFont(new Font("Tahoma", Font.PLAIN, 23));
		
		lblResultado = new JLabel("");
		lblResultado.setBounds(13, 60, 389, 336);
		panelResultadoCompleto.add(lblResultado);
		lblResultado.setBorder(null);
		lblResultado.setVerticalAlignment(SwingConstants.TOP);
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		lblResultadoInfo = new JLabel();
		lblResultadoInfo.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Info", TitledBorder.CENTER, TitledBorder.BOTTOM, null, Color.GRAY));
		lblResultadoInfo.setBounds(445, 2, 180, 249);
		panelResultado.add(lblResultadoInfo);
		
		btnMostrarGraficos = new JButton("");
		btnMostrarGraficos.setBounds(483, 265, 100, 100);
		panelResultado.add(btnMostrarGraficos);
		btnMostrarGraficos.setIcon(new ImageIcon(MenuPrincipalGUI.class.getResource("/images/icon_graf.png")));
		
		lblVerGrfico = new JLabel("Ver gr�fico");
		lblVerGrfico.setHorizontalAlignment(SwingConstants.CENTER);
		lblVerGrfico.setBounds(443, 371, 170, 14);
		panelResultado.add(lblVerGrfico);
		
		panelEstado = new JPanel();
		panelEstado.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelEstado.setName("");
		panelEstado.setBounds(11, 407, 761, 21);
		panelEstado.setLayout(null);

		lblEstado = new JLabel(Constantes.NOMBRE_APLICACION);
		lblEstado.setBounds(10, 0, 526, 19);
		panelEstado.add(lblEstado);

		
		this.getContentPane().add(panelMenu);
		this.getContentPane().add(panelFormulario);
		this.getContentPane().add(panelResultado);
		this.getContentPane().add(panelEstado);
		
		
		 /**
		  *  ---------------- LISTENERS -------------------
		  */
		btnCargarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelFormulario.setVisible(true);
				lblInfoPersona.setVisible(true);
				panelResultado.setVisible(false);
			}
		});
		
		btnCalcularGrupos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelFormulario.setVisible(false);
				panelResultado.setVisible(true);
				ejecutarProceso();
				infoPromedios();
			}

		});
		
		btnMostrarGraficos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GraficoGUI grafico = new GraficoGUI(arbolMinimo);
				grafico.setVisible(true);
				
			}
		});
		
		listPersonas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Persona p = listPersonas.getSelectedValue();
				lblInfoPersona.setText("<html>Indice Deportes: " + p.getIndiceDeportes() + "<p>Indice M�sica: "
						+ p.getIndiceMusica() + "<p>Indice Espect�culos: " + p.getIndiceEspectaculos()
						+ "<p>Indice Ciencia: " + p.getIndiceCiencia() + "<html>");
				}
			}
		);
		
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					if (validarDatos()) {
						Persona p = obtenerPersonaFormulario();
						agregarPersonaJLista(p);
						limpiarFormulario();
					}
					
					if (cantidadLista() >= 2)
					btnCalcularGrupos.setEnabled(true);
				}
			});
		
		mntmBorrar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			    int index = listPersonas.getSelectedIndex();
			    if (index != -1) {
			    	eliminarPersonaJLista(index);
			}
		}});

	}

	
	protected void ejecutarProceso() {
		//Genera un grafo completo con las personas ingresadas
		grafoPersonas.completarGrafo();
		
		//Obtiene el arbol minimo del grafo de personas
		arbolMinimo = Algoritmos.arbolMinimo(grafoPersonas);
		
		//Elimina la relacion/arista de mayor peso del grafo
		arbolMinimo.eliminarRelacionMayorPeso();
		
		//Muestra por pantalla el resultado
		escribirResultado(arbolMinimo);
		
	}

	private void escribirResultado(GrafoPersonas arbol) {
		ArrayList<Set<Persona>> grupos = Algoritmos.devolverGruposAfines(arbol);
		int index = 1;
		StringBuilder sb = new StringBuilder(Constantes.TAG_HTML_I);
		
		for (Set<Persona> grupo : grupos) {
			sb.append("<u>");
			sb.append(Constantes.GRUPO_AFINIDAD+index).append(Constantes.TAG_HTML_NUEVA_LINEA);
			sb.append("</u>");
			for (Persona persona : grupo) {
				sb.append(persona.getNombre());
				sb.append(Constantes.TAG_HTML_NUEVA_LINEA);
			}
			sb.append(Constantes.TAG_HTML_NUEVA_LINEA);
			index++;
		}
		sb.append(Constantes.TAG_HTML_F);
		lblResultado.setText(sb.toString());
	}

	private boolean validarDatos() {

		if (textFieldNombre.getText() == null || textFieldNombre.getText().isEmpty()) {
			escribirEstado(Constantes.ERROR_DATOS_NOMBRE, Constantes.TIPO_ESTADO_ERROR);
			return false;
		}
		if (buttonGroupDeportes.getSelection() == null) {
			escribirEstado(Constantes.ERROR_DATOS_DEPORTE, Constantes.TIPO_ESTADO_ERROR);
			return false;
		}
		if (buttonGroupMusica.getSelection() == null) {
			escribirEstado(Constantes.ERROR_DATOS_MUSICA, Constantes.TIPO_ESTADO_ERROR);
			return false;
		}
		if (buttonGroupEspectaculos.getSelection() == null) {
			escribirEstado(Constantes.ERROR_DATOS_ESPECTACULO, Constantes.TIPO_ESTADO_ERROR);
			return false;
		}
		if (buttonGroupCiencia.getSelection() == null) {
			escribirEstado(Constantes.ERROR_DATOS_CIENCIA, Constantes.TIPO_ESTADO_ERROR);
			return false;
		}

		return true;
	}

	private void escribirEstado(String estado, int tipoEstado) {

		if (tipoEstado == Constantes.TIPO_ESTADO_INFO)
			lblEstado.setForeground(Color.BLACK);
		if (tipoEstado == Constantes.TIPO_ESTADO_ERROR)
			lblEstado.setForeground(Color.RED);
		if (tipoEstado == Constantes.TIPO_ESTADO_OK)
			lblEstado.setForeground(Color.GREEN);

		lblEstado.setText(estado);
	}

	private void limpiarFormulario() {
		textFieldNombre.setText(Constantes.STRING_VACIO);
		buttonGroupDeportes.clearSelection();
		buttonGroupMusica.clearSelection();
		buttonGroupEspectaculos.clearSelection();
		buttonGroupCiencia.clearSelection();
		escribirEstado(Constantes.NOMBRE_APLICACION, Constantes.TIPO_ESTADO_INFO);
	}

	private int cantidadLista() {
		return listPersonas.getModel().getSize();
	}

	private int getIDPersona() {
		return cantidadLista() + 1;
	}

	private Persona obtenerPersonaFormulario() {
		int indentificador = getIDPersona();
		String nombre = textFieldNombre.getText();
		int indiceDeporte = Integer.parseInt(buttonGroupDeportes.getSelection().getActionCommand());
		int indiceMusica = Integer.parseInt(buttonGroupMusica.getSelection().getActionCommand());
		int indiceEspectaculo = Integer.parseInt(buttonGroupEspectaculos.getSelection().getActionCommand());
		int indiceCiencia = Integer.parseInt(buttonGroupCiencia.getSelection().getActionCommand());

		Persona p = new Persona(indentificador, nombre, indiceDeporte, indiceMusica, indiceEspectaculo, indiceCiencia);
		return p;
	}

	private void agregarPersonaJLista(Persona p) {
		DefaultListModel<Persona> model = (DefaultListModel<Persona>) listPersonas.getModel();
		model.addElement(p);
		grafoPersonas.agregarPersona(p);
		listPersonas.setModel(model);
		escribirEstado(Constantes.PERSONA_AGREGADA, Constantes.TIPO_ESTADO_OK);
	}
	
	private void eliminarPersonaJLista(int index) {
		DefaultListModel<Persona> model = (DefaultListModel<Persona>) listPersonas.getModel();
		grafoPersonas.eliminarPersona(model.get(index));
		model.removeElementAt(index);
		listPersonas.setModel(model);
		escribirEstado(Constantes.PERSONA_BORRADA, Constantes.TIPO_ESTADO_OK);
	}

	private Set<Persona> personasCargadas() {
		return this.grafoPersonas.obtenerTodos();
	}

	private void infoPromedios() {
		DecimalFormat df = new DecimalFormat("#.#");

		lblResultadoInfo.setText("<html>Promedio General: "
				+ df.format(Algoritmos.similaridadPromedio(personasCargadas())) + "<p><p>Promedio Deportes:"
				+ df.format(Algoritmos.promedioDeportes(personasCargadas())) + "<p>Promedio M�sica:"
				+ df.format(Algoritmos.promedioMusica(personasCargadas())) + "<p>Promedio Espect�culos:"
				+ df.format(Algoritmos.promedioEspectaculo(personasCargadas())) + "<p>Promedio Ciencia:"
				+ df.format(Algoritmos.promedioCiencia(personasCargadas())) + "<html>");
	}
}
