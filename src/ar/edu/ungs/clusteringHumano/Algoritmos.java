package ar.edu.ungs.clusteringHumano;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Algoritmos {

	public static int calcularSimilaridad(Persona uno, Persona dos) {

		return Math.abs(uno.getIndiceDeportes() - dos.getIndiceDeportes())
				+ Math.abs(uno.getIndiceMusica() - dos.getIndiceMusica())
				+ Math.abs(uno.getIndiceEspectaculos() - dos.getIndiceEspectaculos())
				+ Math.abs(uno.getIndiceCiencia() - dos.getIndiceCiencia());
	}

	public static GrafoPersonas arbolMinimo(GrafoPersonas grafo) {

		GrafoPersonas arbolMinimo = new GrafoPersonas();
		Persona inicial = grafo.obtenerPersona();
		ArrayList<Persona> personasAgregadas = new ArrayList<Persona>();
		int i = 1;

		arbolMinimo.agregarPersona(inicial);

		while (i <= grafo.tamanio() - 1) {

			personasAgregadas.add(inicial);
			Persona afin = grafo.vecinoAfin(inicial, personasAgregadas);
			arbolMinimo.agregarPersona(afin);
			arbolMinimo.agregarRelacion(inicial, afin);

			inicial = afin;
			i++;

		}

		return arbolMinimo;

	}

	public static HashSet<Persona> alcanzables(GrafoPersonas grafo) {
		ArrayList<Persona> vertices = new ArrayList<>();
		HashSet<Persona> verticesMarcados = new HashSet<>();
		Persona p = grafo.obtenerPersona();
		vertices.add(p);
		int index = 0;
		while (!vertices.isEmpty()) {
			Persona i = vertices.get(index);
			verticesMarcados.add(i);
			for (Persona vecino : grafo.devolverVecinos(i)) {
				if (!verticesMarcados.contains(vecino)) {
					vertices.add(vecino);
				}
			}
			vertices.remove(i);
		}
		return verticesMarcados;
	}

	
	@SuppressWarnings("unchecked")
	public static ArrayList<Set<Persona>> devolverGruposAfines(GrafoPersonas arbolMinimo) {

		ArrayList<Set<Persona>> grupos = new ArrayList<Set<Persona>>();

		HashSet<Persona> componenteConexa1 = alcanzables(arbolMinimo);
		HashSet<Persona> todos = arbolMinimo.obtenerPersonas();
		
		HashSet<Persona> grupo1 = (HashSet<Persona>) componenteConexa1.clone();
		HashSet<Persona> grupo2 = (HashSet<Persona>) todos.clone();
		
		grupo2.removeAll(grupo1);

		grupos.add(grupo1);
		grupos.add(grupo2);

		return grupos;
	}

	public static double promedioCiencia(Set<Persona> integrantes) {
		double prom = 0;
		for (Persona persona : integrantes) {
			prom = prom + persona.getIndiceCiencia();
		}

		return prom / integrantes.size();
	}

	public static double promedioEspectaculo(Set<Persona> integrantes) {
		double prom = 0;
		for (Persona persona : integrantes) {
			prom = prom + persona.getIndiceEspectaculos();
		}

		return prom / integrantes.size();
	}

	public static double promedioDeportes(Set<Persona> integrantes) {
		double prom = 0;
		for (Persona persona : integrantes) {
			prom = prom + persona.getIndiceDeportes();
		}

		return prom / integrantes.size();
	}

	public static double promedioMusica(Set<Persona> integrantes) {
		double prom = 0;
		for (Persona persona : integrantes) {
			prom = prom + persona.getIndiceMusica();
		}

		return prom / integrantes.size();
	}

	public static double similaridadPromedio(Set<Persona> integrantes) {
		double prom = promedioCiencia(integrantes) + promedioDeportes(integrantes) + promedioEspectaculo(integrantes)
				+ promedioMusica(integrantes);
		return prom / 4;
	}

}
