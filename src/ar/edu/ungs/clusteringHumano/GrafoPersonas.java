package ar.edu.ungs.clusteringHumano;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class GrafoPersonas {

	// Representamos el grafo por listas de vecinos
	private HashMap<Persona, HashSet<Persona>> vecinos;

	// Constructor
	public GrafoPersonas() {
		this.vecinos = new HashMap<Persona, HashSet<Persona>>();
	}

	// M�todos de personas
	public void agregarPersona(Persona T) {
		HashSet<Persona> aux = new HashSet<Persona>();
		vecinos.put(T, aux);
	}

	public void eliminarPersona(Persona T) {
		vecinos.remove(T);
	}

	public boolean existePersona(Persona T) {
		return vecinos.containsKey(T);
	}

	// M�todos de relaciones
	public void agregarRelacion(Persona T, Persona V) {
		if (T.equals(V))
			throw new IllegalArgumentException("No se puede agregar una relaci�n sobre si mismo");
		vecinos.get(T).add(V);
		vecinos.get(V).add(T);
	}

	public void eliminarRelacion(Persona T, Persona V) {
		vecinos.get(T).remove(V);
		vecinos.get(V).remove(T);
	}

	public boolean existeRelacion(Persona V, Persona T) {
		return vecinos.get(V).contains(T);
	}

	// pero come de todo� pero con m�todos. Con m�todos pocos, claro� claros, con
	// m�todos poco claros
	public HashSet<Persona> devolverVecinos(Persona T) {
		return vecinos.get(T);
	}

	public void completarGrafo() {
		for (Persona T : vecinos.keySet()) {
			for (Persona V : vecinos.keySet()) {
				if (!T.equals(V))
					agregarRelacion(T, V);
			}
		}
	}

	public int tamanio() {
		return vecinos.size();
	}

	public Persona vecinoAfin(Persona P, ArrayList<Persona> marcados) {

		Integer menor = 1000;
		HashSet<Persona> vecinos = devolverVecinos(P);
		Persona candidato = null;

		for (Persona vecino : vecinos) {
			Integer valor = Algoritmos.calcularSimilaridad(P, vecino);
			if (valor < menor &&  !marcados.contains(vecino)) {
				menor = valor;
				candidato = vecino;
			}
		}

		return candidato;
	}

	public void eliminarRelacionMayorPeso(){
		Persona persona1 = null;
		Persona persona2 = null;
		
		int mayor = -1;
		
		for (Persona T : vecinos.keySet()) {
			for (Persona V : devolverVecinos(T)) {
				int peso = Algoritmos.calcularSimilaridad(T, V);
				if(peso > mayor) {
					mayor = peso;
					persona1 = T;
					persona2 = V;
				}
			}
		}
		
		eliminarRelacion(persona1, persona2);
		
	}
	
	public Persona obtenerPersona() {
		return vecinos.keySet().stream().findFirst().get();
	}
	
	public Set<Persona> obtenerTodos() {
		return vecinos.keySet();
	}
	
	public HashSet<Persona> obtenerPersonas() {
		HashSet<Persona> todasPersonas = new HashSet<Persona>();
		todasPersonas.addAll(vecinos.keySet());
		return todasPersonas;
	}
	
	
	public Boolean esConexo() {
		return Algoritmos.alcanzables(this).size() == this.tamanio();
	}

	@Override
	public String toString() {
		return "GrafoPersonas [vecinos=" + vecinos + "]";
	}
	
	
	
}
