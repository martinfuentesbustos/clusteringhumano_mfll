package ar.edu.ungs.clusteringHumano.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;

public class MotorGrafico {

	
	private static Graphics2D getGraficos2D(Graphics g) {
		return (Graphics2D)g;
	}

	public static void dibujarCirculo(Graphics g, int x, int y, int ancho, int alto) {
		getGraficos2D(g).drawOval(x, y, ancho, alto);
	}
	
	public static void dibujarCirculoShape(Graphics g, int x, int y, int ancho, int alto) {
		Shape circulo = new Ellipse2D.Double(alto, ancho, x, y);
		getGraficos2D(g).draw(circulo);
	}
	
	public static void dibujarLinea(Graphics g, int x1, int x2, int y1, int y2) {
		getGraficos2D(g).drawLine(x1, y1, x2, y2);
	}
	
	public static void escribirTexto(Graphics g, String texto, int x, int y) {
		getGraficos2D(g).drawString(texto, x, y);
	}
	
	public static void cambiarFuente(Graphics g, Font fuente) {
		getGraficos2D(g).setFont(fuente);
	}
	
	public static void ponerColorTrazo(Graphics g, Color color){
		getGraficos2D(g).setColor(color);
	}
	
	public static void ponerTrazo(Graphics g, Stroke trazo){
		getGraficos2D(g).setStroke(trazo);
	}

}
