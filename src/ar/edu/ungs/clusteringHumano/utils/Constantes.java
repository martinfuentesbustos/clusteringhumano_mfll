package ar.edu.ungs.clusteringHumano.utils;

public class Constantes {

	public static final String NOMBRE_APLICACION = "Clustering Humano 98SE";

	public static final Integer ESPACIO_DEFAULT = 10;
	public static final Integer ALTO_CARACTER = 6;
	public static final Integer ANCHO_CARACTER = 3;
	public static final String STRING_VACIO = "";
	
	public static final int TIPO_ESTADO_INFO = 0; //negro
	public static final int TIPO_ESTADO_ERROR = 1; //rojo
	public static final int TIPO_ESTADO_OK = 2; // verde
	
	public static final String ERROR_DATOS_NOMBRE = "Por favor complete el nombre de la persona";
	public static final String ERROR_DATOS_DEPORTE = "Por favor complete el valor correspondiente a deportes";
	public static final String ERROR_DATOS_MUSICA = "Por favor complete el valor correspondiente a m�sica";
	public static final String ERROR_DATOS_ESPECTACULO = "Por favor complete el valor correspondiente a espect�culo";
	public static final String ERROR_DATOS_CIENCIA = "Por favor complete el valor correspondiente a ciencia";
	public static final String PERSONA_AGREGADA = "La persona ha sido agregada con �xito!";
	public static final String PERSONA_BORRADA = "La persona ha sido borrada con �xito!";
	
	public static final String TAG_HTML_I = "<HTML>";
	public static final String TAG_HTML_F = "</HTML>";
	public static final String TAG_HTML_NUEVA_LINEA = "<BR>";

	public static final String GRUPO_AFINIDAD = "Grupo de afinidad ";
}
